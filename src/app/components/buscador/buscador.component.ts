import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HeroesService } from 'src/app/servicios/heroes.service';

@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.component.html',
  styleUrls: ['./buscador.component.css']
})
export class BuscadorComponent implements OnInit {

  //Preguntar por que no se puede con heroe
  heroes: any[] = [];
  termino!: string;

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private heroesService: HeroesService) { }
  ngOnInit(): void {//Obtener una variable por la url
    this.activatedRoute.params.subscribe(params => {
      console.log(params['termino']);
      this.termino = params['termino'];

      this.heroes = this.heroesService.buscarHeroes(params['termino']);
      console.log(this.heroes);
    });
  }
  verHeroe(idx: number){
    console.log(idx);
    this.router.navigate(['/heroe',idx]);
    
  }

}
